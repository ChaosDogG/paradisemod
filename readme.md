# Paradise Mod 2
![](logo.png)

Paradise Mod 2 will be a full rewrite of Nether Noah's Paradise Mod for Minecraft versions 1.16.5 and forward. This rewrite will port over old features and bring in brand new ones!

[website](https://www.paradisemod.net/)
